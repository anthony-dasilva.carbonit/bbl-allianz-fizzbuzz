package com.kawabytes.kata;

public interface FizzBuzzAlgorithm {
    String compute(int value);
}
