package com.kawabytes.kata;

public final class DefaultFizzBuzzAlgorithm implements FizzBuzzAlgorithm {
    private final String MultipleOf3Value = "Fizz";
    private final String MultipleOf5Value = "Buzz";
    private final String MultipleOf3And5Value = "FizzBuzz";

    @Override
    public String compute(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException("value is less than 1");
        }

        final var isMultipleOf3 = value % 3 == 0;
        final var isMultipleOf5 = value % 5 == 0;

        // Intentional verbose conditionals to be explicit.
        if (isMultipleOf3 && isMultipleOf5)
            return MultipleOf3And5Value;

        if (isMultipleOf3 && !isMultipleOf5)
            return MultipleOf3Value;

        if (!isMultipleOf3 && isMultipleOf5)
            return MultipleOf5Value;

        // 🤫 : unspoken rule.
        //if(!isMultipleOf3 && !isMultipleOf5)
        return Integer.valueOf(value).toString();
    }
}
