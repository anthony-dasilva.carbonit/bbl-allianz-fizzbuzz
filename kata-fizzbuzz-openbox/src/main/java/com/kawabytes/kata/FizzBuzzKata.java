package com.kawabytes.kata;

import java.io.PrintStream;
import java.util.stream.IntStream;

public final class FizzBuzzKata {
    private final FizzBuzzAlgorithm fizzBuzzAlgorithm;
    private final PrintStream printStream;

    public FizzBuzzKata(FizzBuzzAlgorithm fizzBuzzAlgorithm, PrintStream printStream) {
        this.fizzBuzzAlgorithm = fizzBuzzAlgorithm;
        this.printStream = printStream;
    }

    public void printFizzBuzz(int start, int count) {
        guardAgainstOutOfRangeArgument(start);
        guardAgainstOutOfRangeArgument(count);

        IntStream.range(start, start + count)
                 .forEach(x -> printStream.println(fizzBuzzAlgorithm.compute(x)));
    }

    private static void guardAgainstOutOfRangeArgument(int input) {
        if(input <= 0) {
            throw new IllegalArgumentException("input is less than 1");
        }
    }
}

