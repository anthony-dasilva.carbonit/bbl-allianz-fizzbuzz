import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FizzBuzzKataTest {
    private ByteArrayOutputStream consoleOutputStream;
    private PrintStream formerOutputPrintStream;

    @BeforeEach
    void redirectOutputStream() {
        consoleOutputStream = new ByteArrayOutputStream();
        formerOutputPrintStream = System.out;
        System.setOut(new PrintStream(consoleOutputStream));
    }

    @Test
    void kataUserStory() throws IOException, URISyntaxException {
        // 1. Arrange
        final var goldenMasterPath = Path.of(
                FizzBuzzKataTest.class.getClassLoader()
                .getResource("kataUserStory.expected")
                .toURI());

        final var expected = Files.readString(goldenMasterPath);

        // 2. Act
        Program.main(null);

        // 3. Assert
        final var actual = new String(consoleOutputStream.toByteArray());

        Assertions.assertEquals(expected, actual);
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(formerOutputPrintStream);
    }

    // ⚠️ not enough, bad naming ! ;)
    @Test
    void givenAMultipleOf3ShouldReturnFizz() {
        // 1. Arrange
        final var expected = "Fizz";
        final var input = 3;

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }

    // ⚠️ not enough, bad naming ! ;)
    @Test
    void givenAMultipleOf5ShouldReturnBuzz() {
        // 1. Arrange
        final var expected = "Buzz";
        final var input = 5;

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }

    // ⚠️ not enough, bad naming ! ;)
    @Test
    void givenAMultipleOf3And5ShouldReturnFizzBuzz() {
        // 1. Arrange
        final var expected = "FizzBuzz";
        final var input = 15;

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }

    // ⚠️ not enough, bad naming ! ;)
    @Test
    void givenAnyOtherValueShouldReturnTheValue() {
        // 1. Arrange
        final var expected = "1";
        final var input = 1;

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }

    // ⚠️ enough but skipping values, bad naming ! ;)
    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9, 12, /*15, <- uh oh */ 18, 21})
    void givenSomeMultipleOf3ShouldReturnFizz(int input) {
        // 1. Arrange
        final var expected = "Fizz";

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }

    // ⚠️ enough but bad naming ! ;)
    @ParameterizedTest
    @CsvSource({"1, 1", "2, 2", "3, Fizz", "4, 4", "5, Buzz", "15, FizzBuzz"})
    void givenSomeValueShouldReturnExpectedValue(int input, String expected) {
        // 1. Arrange via @CsvSource

        // 2. Act
        final var actual = Program.compute(input);

        // 3. Assert
        Assertions.assertEquals(expected, actual);
    }
}
