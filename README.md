# Florilège de techniques de test en Java
## BBL - Allianz - 07/07/2020, présenté par [Maxime MADER](https://twitter.com/kawabytes)

### Le kata Fizz Buzz
Il s’agit d’un kata célèbre utilisé dans les entretiens techniques. Je vous propose de consulter [l’article issu du wiki de Cunningham & Cunningham](https://wiki.c2.com/?FizzBuzzTest) qui centralise quelques échanges et articles intéressants autour du kata et de son utilisation en entretien technique.

Voici les règles métiers :
- Write a program that prints one line for each number from 1 to 100
- For multiples of three print Fizz instead of the number
- For the multiples of five print Buzz instead of the number
- For numbers which are multiples of both three and five print FizzBuzz instead of the number

Il existe également [une variante intéressante appelée FooBarQix](https://codingdojo.org/kata/FooBarQix/) qui ajoute un peu de complexité, justifiant une implémentation plus élaborée.

### Approche “boîte noire”
L’idée de [cette approche](https://en.wikipedia.org/wiki/Black-box_testing), c’est d’examiner notre application sans connaissance de sa structure interne, sans fouiller dans les détails de son implémentation. Notre “boîte” logicielle étant fermée, nous devons nous contenter des entrées et des sorties directes. 

Généralement, les cas de tests sont élaborés à partir des spécifications et exigences de l’application.

### Approche “boîte blanche”
L’idée de [cette approche](https://en.wikipedia.org/wiki/White-box_testing), c’est d’examiner notre application en ayant connaissance de sa structure internes ainsi que des détails de son implémentation. Notre “boîte” logicielle étant ouverte, nous pouvons nous intéresser aux interactions entre les différentes briques de notre code, à la couverture du code, aux différentes branches de notre code, etc.

Bien que cette approche nous permet de tester notre code sous de nombreuses facettes, cette stratégie de tests apporte son lot de complexité : le code doit être raisonnablement testable, les tests ne doivent pas être couplés à une implémentation spécifique, les tests ne doivent pas être une duplication du code de l’implémentation, etc.

## Outils de test
### Tests unitaires avec JUnit
[C'est un outil](https://junit.org/junit5/) que l’on ne présente plus, cependant, il est bon de rappeler que JUnit 5 est disponible depuis le 10 septembre 2017 et est à présent, assez mature pour oublier JUnit 4. De plus, JUnit 5 nous permet d’utiliser assez facilement [les tests paramétrés](https://blog.codefx.org/libraries/junit-5-parameterized-tests/) ([documentation officielle](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests)).

Alternatives : [jqwik](https://jqwik.net), [TestNG](https://testng.org/doc/index.html), ...

### Property-Based Testing avec jqwik
[Cet outil](https://jqwik.net) nous permet d’utiliser le [Property-Based Testing](https://jqwik.net/property-based-testing.html) (PBT) sur la JVM. Jqwik nous permet également de fonctionner avec ou sans le moteur de JUnit (Jupiter/Vintage) selon nos envies.

Bon à savoir :
jqwik n’intègre pas de bibliothèque d’assertions, donc libre à vous de choisir Hamcrest ou AssertJ par exemple.
jqwik n’est pas compatible avec les annotations du type @RunWith.

Alternatives : [JUnit QuickCheck](https://pholser.github.io/junit-quickcheck/site/0.9.1/), [Vavr Test](https://github.com/vavr-io/vavr-test), [Scala Check](http://www.scalacheck.org/), …

### Double de tests avec Mockito
[Cet outil](https://site.mockito.org/) nous permet d’utiliser notamment des [mocks](https://en.wikipedia.org/wiki/Mock_object) afin de tester les interactions entre nos différents objets.

Alternatives : BDDMockito, [JMockit](https://jmockit.github.io/), [EasyMock](https://easymock.org/), ...